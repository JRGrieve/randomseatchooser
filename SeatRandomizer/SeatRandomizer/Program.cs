﻿//-------------------------
//-------------------------
//Binary & Serialization Initialization [ICA18]
//-------------------------
//By James Grieve
//-------------------------
//-------------------------
//Description:
//Initializes the form.
//-------------------------
//-------------------------

//-LIBRAIRIES-
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
//-END LIBRAIRIES-

//-INITIALIZATION NAMESPACE-
namespace ICA18_Init {
	//-MAIN CLASS-
	static class Program {
		//---MAIN FUNCTION---
		[STAThread]
		static void Main() {
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new SeatRandomizer.SeatRandomizer());
		}
		//---END MAIN FUNCTION---
	}
	//-END MAIN CLASS-
}
//-END INITIALIZATION NAMESPACE-