﻿namespace SeatRandomizer
{
    partial class SeatRandomizer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SeatRandomizer));
            this.ContextMenu_Marker = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ContextMenu_Marker_AddGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Marker_AddGroup_2_V = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Marker_AddGroup_2_VL = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Marker_AddGroup_2_VR = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Marker_AddGroup_2_H = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Marker_AddGroup_2_HL = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Marker_AddGroup_2_HR = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Marker_AddGroup_4_S = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Marker_AddGroup_4_SL = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Marker_AddGroup_4_SR = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Marker_AddGroup_6_V = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Marker_AddGroup_6_VL = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Marker_AddGroup_6_VR = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Marker_AddGroup_6_H = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Marker_AddGroup_6_HL = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Marker_AddGroup_6_HR = new System.Windows.Forms.ToolStripMenuItem();
            this.addObjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teachersDeskNorthFacingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teachersDeskWestFacingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teachersDeskEastFacingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teachersDeskSouthFacingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowVerticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowHorizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.whiteBoardVerticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.whiteBoardHorizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smartBoardVerticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smartBoardVerticalEastFacingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smartBoardHorizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Desk = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ContextMenu_Desk_Remove = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenu_Object = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeObjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TextBox_Randomize = new System.Windows.Forms.TextBox();
            this.Button_Randomize = new System.Windows.Forms.Button();
            this.Button_Clear = new System.Windows.Forms.Button();
            this.Button_Reset = new System.Windows.Forms.Button();
            this.Marker0 = new System.Windows.Forms.Button();
            this.Marker1 = new System.Windows.Forms.Button();
            this.Marker3 = new System.Windows.Forms.Button();
            this.Marker7 = new System.Windows.Forms.Button();
            this.Marker8 = new System.Windows.Forms.Button();
            this.Marker10 = new System.Windows.Forms.Button();
            this.Marker2 = new System.Windows.Forms.Button();
            this.Marker9 = new System.Windows.Forms.Button();
            this.Marker14 = new System.Windows.Forms.Button();
            this.Marker15 = new System.Windows.Forms.Button();
            this.Marker16 = new System.Windows.Forms.Button();
            this.Marker17 = new System.Windows.Forms.Button();
            this.SeatRandomizerGroupBox = new System.Windows.Forms.GroupBox();
            this.CheckBox_AllBeforeRechoose = new System.Windows.Forms.CheckBox();
            this.Marker27 = new System.Windows.Forms.Button();
            this.Marker26 = new System.Windows.Forms.Button();
            this.Marker25 = new System.Windows.Forms.Button();
            this.Marker20 = new System.Windows.Forms.Button();
            this.Marker19 = new System.Windows.Forms.Button();
            this.Marker18 = new System.Windows.Forms.Button();
            this.Marker12 = new System.Windows.Forms.Button();
            this.Marker5 = new System.Windows.Forms.Button();
            this.Marker13 = new System.Windows.Forms.Button();
            this.Marker11 = new System.Windows.Forms.Button();
            this.Marker6 = new System.Windows.Forms.Button();
            this.Marker4 = new System.Windows.Forms.Button();
            this.Marker24 = new System.Windows.Forms.Button();
            this.Marker23 = new System.Windows.Forms.Button();
            this.Marker22 = new System.Windows.Forms.Button();
            this.Marker21 = new System.Windows.Forms.Button();
            this.CheckBox_LengthLimit = new System.Windows.Forms.CheckBox();
            this.SeatRandomizerMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveLayoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveLayoutAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLayoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miscToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkForUpdatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.creditsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.CheckBox_FlashChosen = new System.Windows.Forms.CheckBox();
            this.ContextMenu_Marker.SuspendLayout();
            this.ContextMenu_Desk.SuspendLayout();
            this.ContextMenu_Object.SuspendLayout();
            this.SeatRandomizerGroupBox.SuspendLayout();
            this.SeatRandomizerMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContextMenu_Marker
            // 
            this.ContextMenu_Marker.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ContextMenu_Marker_AddGroup,
            this.addObjectToolStripMenuItem});
            this.ContextMenu_Marker.Name = "contextMenuStrip1";
            resources.ApplyResources(this.ContextMenu_Marker, "ContextMenu_Marker");
            // 
            // ContextMenu_Marker_AddGroup
            // 
            this.ContextMenu_Marker_AddGroup.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ContextMenu_Marker_AddGroup_2_V,
            this.ContextMenu_Marker_AddGroup_2_VL,
            this.ContextMenu_Marker_AddGroup_2_VR,
            this.ContextMenu_Marker_AddGroup_2_H,
            this.ContextMenu_Marker_AddGroup_2_HL,
            this.ContextMenu_Marker_AddGroup_2_HR,
            this.ContextMenu_Marker_AddGroup_4_S,
            this.ContextMenu_Marker_AddGroup_4_SL,
            this.ContextMenu_Marker_AddGroup_4_SR,
            this.ContextMenu_Marker_AddGroup_6_V,
            this.ContextMenu_Marker_AddGroup_6_VL,
            this.ContextMenu_Marker_AddGroup_6_VR,
            this.ContextMenu_Marker_AddGroup_6_H,
            this.ContextMenu_Marker_AddGroup_6_HL,
            this.ContextMenu_Marker_AddGroup_6_HR});
            this.ContextMenu_Marker_AddGroup.Name = "ContextMenu_Marker_AddGroup";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup, "ContextMenu_Marker_AddGroup");
            // 
            // ContextMenu_Marker_AddGroup_2_V
            // 
            this.ContextMenu_Marker_AddGroup_2_V.Name = "ContextMenu_Marker_AddGroup_2_V";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup_2_V, "ContextMenu_Marker_AddGroup_2_V");
            this.ContextMenu_Marker_AddGroup_2_V.Click += new System.EventHandler(this.Button_CreateGroup_Setup);
            // 
            // ContextMenu_Marker_AddGroup_2_VL
            // 
            this.ContextMenu_Marker_AddGroup_2_VL.Name = "ContextMenu_Marker_AddGroup_2_VL";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup_2_VL, "ContextMenu_Marker_AddGroup_2_VL");
            this.ContextMenu_Marker_AddGroup_2_VL.Click += new System.EventHandler(this.Button_CreateGroup_Setup);
            // 
            // ContextMenu_Marker_AddGroup_2_VR
            // 
            this.ContextMenu_Marker_AddGroup_2_VR.Name = "ContextMenu_Marker_AddGroup_2_VR";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup_2_VR, "ContextMenu_Marker_AddGroup_2_VR");
            this.ContextMenu_Marker_AddGroup_2_VR.Click += new System.EventHandler(this.Button_CreateGroup_Setup);
            // 
            // ContextMenu_Marker_AddGroup_2_H
            // 
            this.ContextMenu_Marker_AddGroup_2_H.Name = "ContextMenu_Marker_AddGroup_2_H";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup_2_H, "ContextMenu_Marker_AddGroup_2_H");
            this.ContextMenu_Marker_AddGroup_2_H.Click += new System.EventHandler(this.Button_CreateGroup_Setup);
            // 
            // ContextMenu_Marker_AddGroup_2_HL
            // 
            this.ContextMenu_Marker_AddGroup_2_HL.Name = "ContextMenu_Marker_AddGroup_2_HL";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup_2_HL, "ContextMenu_Marker_AddGroup_2_HL");
            this.ContextMenu_Marker_AddGroup_2_HL.Click += new System.EventHandler(this.Button_CreateGroup_Setup);
            // 
            // ContextMenu_Marker_AddGroup_2_HR
            // 
            this.ContextMenu_Marker_AddGroup_2_HR.Name = "ContextMenu_Marker_AddGroup_2_HR";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup_2_HR, "ContextMenu_Marker_AddGroup_2_HR");
            this.ContextMenu_Marker_AddGroup_2_HR.Click += new System.EventHandler(this.Button_CreateGroup_Setup);
            // 
            // ContextMenu_Marker_AddGroup_4_S
            // 
            this.ContextMenu_Marker_AddGroup_4_S.Name = "ContextMenu_Marker_AddGroup_4_S";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup_4_S, "ContextMenu_Marker_AddGroup_4_S");
            this.ContextMenu_Marker_AddGroup_4_S.Click += new System.EventHandler(this.Button_CreateGroup_Setup);
            // 
            // ContextMenu_Marker_AddGroup_4_SL
            // 
            this.ContextMenu_Marker_AddGroup_4_SL.Name = "ContextMenu_Marker_AddGroup_4_SL";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup_4_SL, "ContextMenu_Marker_AddGroup_4_SL");
            this.ContextMenu_Marker_AddGroup_4_SL.Click += new System.EventHandler(this.Button_CreateGroup_Setup);
            // 
            // ContextMenu_Marker_AddGroup_4_SR
            // 
            this.ContextMenu_Marker_AddGroup_4_SR.Name = "ContextMenu_Marker_AddGroup_4_SR";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup_4_SR, "ContextMenu_Marker_AddGroup_4_SR");
            this.ContextMenu_Marker_AddGroup_4_SR.Click += new System.EventHandler(this.Button_CreateGroup_Setup);
            // 
            // ContextMenu_Marker_AddGroup_6_V
            // 
            this.ContextMenu_Marker_AddGroup_6_V.Name = "ContextMenu_Marker_AddGroup_6_V";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup_6_V, "ContextMenu_Marker_AddGroup_6_V");
            this.ContextMenu_Marker_AddGroup_6_V.Click += new System.EventHandler(this.Button_CreateGroup_Setup);
            // 
            // ContextMenu_Marker_AddGroup_6_VL
            // 
            this.ContextMenu_Marker_AddGroup_6_VL.Name = "ContextMenu_Marker_AddGroup_6_VL";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup_6_VL, "ContextMenu_Marker_AddGroup_6_VL");
            this.ContextMenu_Marker_AddGroup_6_VL.Click += new System.EventHandler(this.Button_CreateGroup_Setup);
            // 
            // ContextMenu_Marker_AddGroup_6_VR
            // 
            this.ContextMenu_Marker_AddGroup_6_VR.Name = "ContextMenu_Marker_AddGroup_6_VR";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup_6_VR, "ContextMenu_Marker_AddGroup_6_VR");
            this.ContextMenu_Marker_AddGroup_6_VR.Click += new System.EventHandler(this.Button_CreateGroup_Setup);
            // 
            // ContextMenu_Marker_AddGroup_6_H
            // 
            this.ContextMenu_Marker_AddGroup_6_H.Name = "ContextMenu_Marker_AddGroup_6_H";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup_6_H, "ContextMenu_Marker_AddGroup_6_H");
            this.ContextMenu_Marker_AddGroup_6_H.Click += new System.EventHandler(this.Button_CreateGroup_Setup);
            // 
            // ContextMenu_Marker_AddGroup_6_HL
            // 
            this.ContextMenu_Marker_AddGroup_6_HL.Name = "ContextMenu_Marker_AddGroup_6_HL";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup_6_HL, "ContextMenu_Marker_AddGroup_6_HL");
            this.ContextMenu_Marker_AddGroup_6_HL.Click += new System.EventHandler(this.Button_CreateGroup_Setup);
            // 
            // ContextMenu_Marker_AddGroup_6_HR
            // 
            this.ContextMenu_Marker_AddGroup_6_HR.Name = "ContextMenu_Marker_AddGroup_6_HR";
            resources.ApplyResources(this.ContextMenu_Marker_AddGroup_6_HR, "ContextMenu_Marker_AddGroup_6_HR");
            this.ContextMenu_Marker_AddGroup_6_HR.Click += new System.EventHandler(this.Button_CreateGroup_Setup);
            // 
            // addObjectToolStripMenuItem
            // 
            this.addObjectToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.teachersDeskNorthFacingToolStripMenuItem,
            this.teachersDeskWestFacingToolStripMenuItem,
            this.teachersDeskEastFacingToolStripMenuItem,
            this.teachersDeskSouthFacingToolStripMenuItem,
            this.windowVerticalToolStripMenuItem,
            this.windowHorizontalToolStripMenuItem,
            this.whiteBoardVerticalToolStripMenuItem,
            this.whiteBoardHorizontalToolStripMenuItem,
            this.smartBoardVerticalToolStripMenuItem,
            this.smartBoardVerticalEastFacingToolStripMenuItem,
            this.smartBoardHorizontalToolStripMenuItem});
            this.addObjectToolStripMenuItem.Name = "addObjectToolStripMenuItem";
            resources.ApplyResources(this.addObjectToolStripMenuItem, "addObjectToolStripMenuItem");
            // 
            // teachersDeskNorthFacingToolStripMenuItem
            // 
            this.teachersDeskNorthFacingToolStripMenuItem.Name = "teachersDeskNorthFacingToolStripMenuItem";
            resources.ApplyResources(this.teachersDeskNorthFacingToolStripMenuItem, "teachersDeskNorthFacingToolStripMenuItem");
            // 
            // teachersDeskWestFacingToolStripMenuItem
            // 
            this.teachersDeskWestFacingToolStripMenuItem.Name = "teachersDeskWestFacingToolStripMenuItem";
            resources.ApplyResources(this.teachersDeskWestFacingToolStripMenuItem, "teachersDeskWestFacingToolStripMenuItem");
            // 
            // teachersDeskEastFacingToolStripMenuItem
            // 
            this.teachersDeskEastFacingToolStripMenuItem.Name = "teachersDeskEastFacingToolStripMenuItem";
            resources.ApplyResources(this.teachersDeskEastFacingToolStripMenuItem, "teachersDeskEastFacingToolStripMenuItem");
            // 
            // teachersDeskSouthFacingToolStripMenuItem
            // 
            this.teachersDeskSouthFacingToolStripMenuItem.Name = "teachersDeskSouthFacingToolStripMenuItem";
            resources.ApplyResources(this.teachersDeskSouthFacingToolStripMenuItem, "teachersDeskSouthFacingToolStripMenuItem");
            // 
            // windowVerticalToolStripMenuItem
            // 
            this.windowVerticalToolStripMenuItem.Name = "windowVerticalToolStripMenuItem";
            resources.ApplyResources(this.windowVerticalToolStripMenuItem, "windowVerticalToolStripMenuItem");
            // 
            // windowHorizontalToolStripMenuItem
            // 
            this.windowHorizontalToolStripMenuItem.Name = "windowHorizontalToolStripMenuItem";
            resources.ApplyResources(this.windowHorizontalToolStripMenuItem, "windowHorizontalToolStripMenuItem");
            // 
            // whiteBoardVerticalToolStripMenuItem
            // 
            this.whiteBoardVerticalToolStripMenuItem.Name = "whiteBoardVerticalToolStripMenuItem";
            resources.ApplyResources(this.whiteBoardVerticalToolStripMenuItem, "whiteBoardVerticalToolStripMenuItem");
            // 
            // whiteBoardHorizontalToolStripMenuItem
            // 
            this.whiteBoardHorizontalToolStripMenuItem.Name = "whiteBoardHorizontalToolStripMenuItem";
            resources.ApplyResources(this.whiteBoardHorizontalToolStripMenuItem, "whiteBoardHorizontalToolStripMenuItem");
            // 
            // smartBoardVerticalToolStripMenuItem
            // 
            this.smartBoardVerticalToolStripMenuItem.Name = "smartBoardVerticalToolStripMenuItem";
            resources.ApplyResources(this.smartBoardVerticalToolStripMenuItem, "smartBoardVerticalToolStripMenuItem");
            // 
            // smartBoardVerticalEastFacingToolStripMenuItem
            // 
            this.smartBoardVerticalEastFacingToolStripMenuItem.Name = "smartBoardVerticalEastFacingToolStripMenuItem";
            resources.ApplyResources(this.smartBoardVerticalEastFacingToolStripMenuItem, "smartBoardVerticalEastFacingToolStripMenuItem");
            // 
            // smartBoardHorizontalToolStripMenuItem
            // 
            this.smartBoardHorizontalToolStripMenuItem.Name = "smartBoardHorizontalToolStripMenuItem";
            resources.ApplyResources(this.smartBoardHorizontalToolStripMenuItem, "smartBoardHorizontalToolStripMenuItem");
            // 
            // ContextMenu_Desk
            // 
            this.ContextMenu_Desk.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ContextMenu_Desk_Remove});
            this.ContextMenu_Desk.Name = "contextMenuStrip2";
            resources.ApplyResources(this.ContextMenu_Desk, "ContextMenu_Desk");
            // 
            // ContextMenu_Desk_Remove
            // 
            this.ContextMenu_Desk_Remove.Name = "ContextMenu_Desk_Remove";
            resources.ApplyResources(this.ContextMenu_Desk_Remove, "ContextMenu_Desk_Remove");
            this.ContextMenu_Desk_Remove.Click += new System.EventHandler(this.RemoveGroup);
            // 
            // ContextMenu_Object
            // 
            this.ContextMenu_Object.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeObjectToolStripMenuItem});
            this.ContextMenu_Object.Name = "ContextMenu_Object";
            resources.ApplyResources(this.ContextMenu_Object, "ContextMenu_Object");
            // 
            // removeObjectToolStripMenuItem
            // 
            this.removeObjectToolStripMenuItem.Name = "removeObjectToolStripMenuItem";
            resources.ApplyResources(this.removeObjectToolStripMenuItem, "removeObjectToolStripMenuItem");
            // 
            // TextBox_Randomize
            // 
            resources.ApplyResources(this.TextBox_Randomize, "TextBox_Randomize");
            this.TextBox_Randomize.Name = "TextBox_Randomize";
            // 
            // Button_Randomize
            // 
            resources.ApplyResources(this.Button_Randomize, "Button_Randomize");
            this.Button_Randomize.Name = "Button_Randomize";
            this.Button_Randomize.UseVisualStyleBackColor = true;
            this.Button_Randomize.Click += new System.EventHandler(this.Button_Randomize_Click);
            // 
            // Button_Clear
            // 
            resources.ApplyResources(this.Button_Clear, "Button_Clear");
            this.Button_Clear.Name = "Button_Clear";
            this.Button_Clear.UseVisualStyleBackColor = true;
            this.Button_Clear.Click += new System.EventHandler(this.Button_Clear_Click);
            // 
            // Button_Reset
            // 
            resources.ApplyResources(this.Button_Reset, "Button_Reset");
            this.Button_Reset.Name = "Button_Reset";
            this.Button_Reset.UseVisualStyleBackColor = true;
            this.Button_Reset.Click += new System.EventHandler(this.Button_Reset_Click);
            // 
            // Marker0
            // 
            resources.ApplyResources(this.Marker0, "Marker0");
            this.Marker0.Name = "Marker0";
            this.Marker0.UseVisualStyleBackColor = true;
            this.Marker0.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker1
            // 
            resources.ApplyResources(this.Marker1, "Marker1");
            this.Marker1.Name = "Marker1";
            this.Marker1.UseVisualStyleBackColor = true;
            this.Marker1.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker3
            // 
            resources.ApplyResources(this.Marker3, "Marker3");
            this.Marker3.Name = "Marker3";
            this.Marker3.UseVisualStyleBackColor = true;
            this.Marker3.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker7
            // 
            resources.ApplyResources(this.Marker7, "Marker7");
            this.Marker7.Name = "Marker7";
            this.Marker7.UseVisualStyleBackColor = true;
            this.Marker7.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker8
            // 
            resources.ApplyResources(this.Marker8, "Marker8");
            this.Marker8.Name = "Marker8";
            this.Marker8.UseVisualStyleBackColor = true;
            this.Marker8.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker10
            // 
            resources.ApplyResources(this.Marker10, "Marker10");
            this.Marker10.Name = "Marker10";
            this.Marker10.UseVisualStyleBackColor = true;
            this.Marker10.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker2
            // 
            resources.ApplyResources(this.Marker2, "Marker2");
            this.Marker2.Name = "Marker2";
            this.Marker2.UseVisualStyleBackColor = true;
            this.Marker2.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker9
            // 
            resources.ApplyResources(this.Marker9, "Marker9");
            this.Marker9.Name = "Marker9";
            this.Marker9.UseVisualStyleBackColor = true;
            this.Marker9.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker14
            // 
            resources.ApplyResources(this.Marker14, "Marker14");
            this.Marker14.Name = "Marker14";
            this.Marker14.UseVisualStyleBackColor = true;
            this.Marker14.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker15
            // 
            resources.ApplyResources(this.Marker15, "Marker15");
            this.Marker15.Name = "Marker15";
            this.Marker15.UseVisualStyleBackColor = true;
            this.Marker15.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker16
            // 
            resources.ApplyResources(this.Marker16, "Marker16");
            this.Marker16.Name = "Marker16";
            this.Marker16.UseVisualStyleBackColor = true;
            this.Marker16.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker17
            // 
            resources.ApplyResources(this.Marker17, "Marker17");
            this.Marker17.Name = "Marker17";
            this.Marker17.UseVisualStyleBackColor = true;
            this.Marker17.Click += new System.EventHandler(this.MarkerClick);
            // 
            // SeatRandomizerGroupBox
            // 
            resources.ApplyResources(this.SeatRandomizerGroupBox, "SeatRandomizerGroupBox");
            this.SeatRandomizerGroupBox.Controls.Add(this.CheckBox_FlashChosen);
            this.SeatRandomizerGroupBox.Controls.Add(this.CheckBox_AllBeforeRechoose);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker27);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker26);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker25);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker20);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker19);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker18);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker12);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker5);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker13);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker11);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker6);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker4);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker24);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker23);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker22);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker21);
            this.SeatRandomizerGroupBox.Controls.Add(this.CheckBox_LengthLimit);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker17);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker16);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker15);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker14);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker9);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker2);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker10);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker8);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker7);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker3);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker1);
            this.SeatRandomizerGroupBox.Controls.Add(this.Marker0);
            this.SeatRandomizerGroupBox.Controls.Add(this.Button_Reset);
            this.SeatRandomizerGroupBox.Controls.Add(this.Button_Clear);
            this.SeatRandomizerGroupBox.Controls.Add(this.Button_Randomize);
            this.SeatRandomizerGroupBox.Controls.Add(this.TextBox_Randomize);
            this.SeatRandomizerGroupBox.Name = "SeatRandomizerGroupBox";
            this.SeatRandomizerGroupBox.TabStop = false;
            // 
            // CheckBox_AllBeforeRechoose
            // 
            resources.ApplyResources(this.CheckBox_AllBeforeRechoose, "CheckBox_AllBeforeRechoose");
            this.CheckBox_AllBeforeRechoose.Checked = true;
            this.CheckBox_AllBeforeRechoose.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_AllBeforeRechoose.Name = "CheckBox_AllBeforeRechoose";
            this.CheckBox_AllBeforeRechoose.UseVisualStyleBackColor = true;
            // 
            // Marker27
            // 
            resources.ApplyResources(this.Marker27, "Marker27");
            this.Marker27.Name = "Marker27";
            this.Marker27.UseVisualStyleBackColor = true;
            this.Marker27.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker26
            // 
            resources.ApplyResources(this.Marker26, "Marker26");
            this.Marker26.Name = "Marker26";
            this.Marker26.UseVisualStyleBackColor = true;
            this.Marker26.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker25
            // 
            resources.ApplyResources(this.Marker25, "Marker25");
            this.Marker25.Name = "Marker25";
            this.Marker25.UseVisualStyleBackColor = true;
            this.Marker25.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker20
            // 
            resources.ApplyResources(this.Marker20, "Marker20");
            this.Marker20.Name = "Marker20";
            this.Marker20.UseVisualStyleBackColor = true;
            this.Marker20.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker19
            // 
            resources.ApplyResources(this.Marker19, "Marker19");
            this.Marker19.Name = "Marker19";
            this.Marker19.UseVisualStyleBackColor = true;
            this.Marker19.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker18
            // 
            resources.ApplyResources(this.Marker18, "Marker18");
            this.Marker18.Name = "Marker18";
            this.Marker18.UseVisualStyleBackColor = true;
            this.Marker18.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker12
            // 
            resources.ApplyResources(this.Marker12, "Marker12");
            this.Marker12.Name = "Marker12";
            this.Marker12.UseVisualStyleBackColor = true;
            this.Marker12.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker5
            // 
            resources.ApplyResources(this.Marker5, "Marker5");
            this.Marker5.Name = "Marker5";
            this.Marker5.UseVisualStyleBackColor = true;
            this.Marker5.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker13
            // 
            resources.ApplyResources(this.Marker13, "Marker13");
            this.Marker13.Name = "Marker13";
            this.Marker13.UseVisualStyleBackColor = true;
            this.Marker13.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker11
            // 
            resources.ApplyResources(this.Marker11, "Marker11");
            this.Marker11.Name = "Marker11";
            this.Marker11.UseVisualStyleBackColor = true;
            this.Marker11.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker6
            // 
            resources.ApplyResources(this.Marker6, "Marker6");
            this.Marker6.Name = "Marker6";
            this.Marker6.UseVisualStyleBackColor = true;
            this.Marker6.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker4
            // 
            resources.ApplyResources(this.Marker4, "Marker4");
            this.Marker4.Name = "Marker4";
            this.Marker4.UseVisualStyleBackColor = true;
            this.Marker4.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker24
            // 
            resources.ApplyResources(this.Marker24, "Marker24");
            this.Marker24.Name = "Marker24";
            this.Marker24.UseVisualStyleBackColor = true;
            this.Marker24.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker23
            // 
            resources.ApplyResources(this.Marker23, "Marker23");
            this.Marker23.Name = "Marker23";
            this.Marker23.UseVisualStyleBackColor = true;
            this.Marker23.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker22
            // 
            resources.ApplyResources(this.Marker22, "Marker22");
            this.Marker22.Name = "Marker22";
            this.Marker22.UseVisualStyleBackColor = true;
            this.Marker22.Click += new System.EventHandler(this.MarkerClick);
            // 
            // Marker21
            // 
            resources.ApplyResources(this.Marker21, "Marker21");
            this.Marker21.Name = "Marker21";
            this.Marker21.UseVisualStyleBackColor = true;
            this.Marker21.Click += new System.EventHandler(this.MarkerClick);
            // 
            // CheckBox_LengthLimit
            // 
            resources.ApplyResources(this.CheckBox_LengthLimit, "CheckBox_LengthLimit");
            this.CheckBox_LengthLimit.Name = "CheckBox_LengthLimit";
            this.CheckBox_LengthLimit.UseVisualStyleBackColor = true;
            // 
            // SeatRandomizerMenuStrip
            // 
            this.SeatRandomizerMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.miscToolStripMenuItem});
            resources.ApplyResources(this.SeatRandomizerMenuStrip, "SeatRandomizerMenuStrip");
            this.SeatRandomizerMenuStrip.Name = "SeatRandomizerMenuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveLayoutToolStripMenuItem,
            this.saveLayoutAsToolStripMenuItem,
            this.openLayoutToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            resources.ApplyResources(this.fileToolStripMenuItem, "fileToolStripMenuItem");
            // 
            // saveLayoutToolStripMenuItem
            // 
            this.saveLayoutToolStripMenuItem.Name = "saveLayoutToolStripMenuItem";
            resources.ApplyResources(this.saveLayoutToolStripMenuItem, "saveLayoutToolStripMenuItem");
            this.saveLayoutToolStripMenuItem.Click += new System.EventHandler(this.MenuBar_File_Save_Click);
            // 
            // saveLayoutAsToolStripMenuItem
            // 
            this.saveLayoutAsToolStripMenuItem.Name = "saveLayoutAsToolStripMenuItem";
            resources.ApplyResources(this.saveLayoutAsToolStripMenuItem, "saveLayoutAsToolStripMenuItem");
            this.saveLayoutAsToolStripMenuItem.Click += new System.EventHandler(this.MenuBar_File_SaveAs_Click);
            // 
            // openLayoutToolStripMenuItem
            // 
            this.openLayoutToolStripMenuItem.Name = "openLayoutToolStripMenuItem";
            resources.ApplyResources(this.openLayoutToolStripMenuItem, "openLayoutToolStripMenuItem");
            this.openLayoutToolStripMenuItem.Click += new System.EventHandler(this.MenuBar_File_Open_Click);
            // 
            // miscToolStripMenuItem
            // 
            this.miscToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkForUpdatesToolStripMenuItem,
            this.creditsToolStripMenuItem});
            this.miscToolStripMenuItem.Name = "miscToolStripMenuItem";
            resources.ApplyResources(this.miscToolStripMenuItem, "miscToolStripMenuItem");
            // 
            // checkForUpdatesToolStripMenuItem
            // 
            this.checkForUpdatesToolStripMenuItem.Name = "checkForUpdatesToolStripMenuItem";
            resources.ApplyResources(this.checkForUpdatesToolStripMenuItem, "checkForUpdatesToolStripMenuItem");
            this.checkForUpdatesToolStripMenuItem.Click += new System.EventHandler(this.MenuBar_Misc_Updates_Click);
            // 
            // creditsToolStripMenuItem
            // 
            this.creditsToolStripMenuItem.Name = "creditsToolStripMenuItem";
            resources.ApplyResources(this.creditsToolStripMenuItem, "creditsToolStripMenuItem");
            this.creditsToolStripMenuItem.Click += new System.EventHandler(this.MenuBar_Misc_Credits_Click);
            // 
            // OpenFileDialog
            // 
            this.OpenFileDialog.FileName = "openFileDialog1";
            // 
            // CheckBox_FlashChosen
            // 
            resources.ApplyResources(this.CheckBox_FlashChosen, "CheckBox_FlashChosen");
            this.CheckBox_FlashChosen.Name = "CheckBox_FlashChosen";
            this.CheckBox_FlashChosen.UseVisualStyleBackColor = true;
            // 
            // SeatRandomizer
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.SeatRandomizerMenuStrip);
            this.Controls.Add(this.SeatRandomizerGroupBox);
            this.Name = "SeatRandomizer";
            this.ContextMenu_Marker.ResumeLayout(false);
            this.ContextMenu_Desk.ResumeLayout(false);
            this.ContextMenu_Object.ResumeLayout(false);
            this.SeatRandomizerGroupBox.ResumeLayout(false);
            this.SeatRandomizerGroupBox.PerformLayout();
            this.SeatRandomizerMenuStrip.ResumeLayout(false);
            this.SeatRandomizerMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip ContextMenu_Marker;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup;
        private System.Windows.Forms.ContextMenuStrip ContextMenu_Desk;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Desk_Remove;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup_4_S;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup_6_V;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup_2_V;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup_2_H;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup_6_H;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup_2_VL;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup_2_VR;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup_2_HL;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup_2_HR;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup_4_SL;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup_4_SR;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup_6_VL;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup_6_VR;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup_6_HL;
        private System.Windows.Forms.ToolStripMenuItem ContextMenu_Marker_AddGroup_6_HR;
        private System.Windows.Forms.ToolStripMenuItem addObjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem teachersDeskNorthFacingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem teachersDeskWestFacingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem teachersDeskEastFacingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem teachersDeskSouthFacingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowVerticalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowHorizontalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem whiteBoardVerticalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem whiteBoardHorizontalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smartBoardVerticalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smartBoardVerticalEastFacingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smartBoardHorizontalToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip ContextMenu_Object;
        private System.Windows.Forms.ToolStripMenuItem removeObjectToolStripMenuItem;
        private System.Windows.Forms.TextBox TextBox_Randomize;
        private System.Windows.Forms.Button Button_Randomize;
        private System.Windows.Forms.Button Button_Clear;
        private System.Windows.Forms.Button Button_Reset;
        private System.Windows.Forms.Button Marker0;
        private System.Windows.Forms.Button Marker1;
        private System.Windows.Forms.Button Marker3;
        private System.Windows.Forms.Button Marker7;
        private System.Windows.Forms.Button Marker8;
        private System.Windows.Forms.Button Marker10;
        private System.Windows.Forms.Button Marker2;
        private System.Windows.Forms.Button Marker9;
        private System.Windows.Forms.Button Marker14;
        private System.Windows.Forms.Button Marker15;
        private System.Windows.Forms.Button Marker16;
        private System.Windows.Forms.Button Marker17;
        private System.Windows.Forms.GroupBox SeatRandomizerGroupBox;
        private System.Windows.Forms.MenuStrip SeatRandomizerMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveLayoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openLayoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miscToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkForUpdatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem creditsToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog OpenFileDialog;
        private System.Windows.Forms.SaveFileDialog SaveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem saveLayoutAsToolStripMenuItem;
        private System.Windows.Forms.CheckBox CheckBox_LengthLimit;
        private System.Windows.Forms.Button Marker27;
        private System.Windows.Forms.Button Marker26;
        private System.Windows.Forms.Button Marker25;
        private System.Windows.Forms.Button Marker20;
        private System.Windows.Forms.Button Marker19;
        private System.Windows.Forms.Button Marker18;
        private System.Windows.Forms.Button Marker12;
        private System.Windows.Forms.Button Marker5;
        private System.Windows.Forms.Button Marker13;
        private System.Windows.Forms.Button Marker11;
        private System.Windows.Forms.Button Marker6;
        private System.Windows.Forms.Button Marker4;
        private System.Windows.Forms.Button Marker24;
        private System.Windows.Forms.Button Marker23;
        private System.Windows.Forms.Button Marker22;
        private System.Windows.Forms.Button Marker21;
        private System.Windows.Forms.CheckBox CheckBox_AllBeforeRechoose;
        private System.Windows.Forms.CheckBox CheckBox_FlashChosen;
    }
}

