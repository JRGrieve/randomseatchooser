﻿//-------------------------
//-------------------------
//Social Studies Software
//-------------------------
//By James Grieve
//-------------------------
//-------------------------
//Description:
//An interface and management system for note-taking and studying social philosophers and ideologies.
//-------------------------
//-------------------------

//-LIBRAIRIES-
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net;
using System.Diagnostics;
using System.Timers;
//-END LIBRAIRIES-

//-FORM NAMESPACE-
namespace SeatRandomizer {
    //-FORM CLASS-
    public partial class SeatRandomizer : Form {
        //-PREAMBLE FUNCTION-
        static void Preamble() {
            Console.WriteLine("-------------------------");
            Console.WriteLine("-------------------------");
            Console.WriteLine("Program Successfully Initialized.");
            Console.WriteLine("-------------------------");
            Console.WriteLine("-------------------------");
        }
        //-END PREAMBLE FUNCTION-

        //-GLOBAL FORM CLASS VARIABLES-
        WebClient WebParser = new WebClient();
        Random RandomGen = new Random();
        BinaryFormatter BinarySerializer = new BinaryFormatter();
        String sSoftwareVersion = "1.00";
        String sUpdateServer = "http://jamesonrgrieve.site.nfoservers.com/versioning/seatrandomizer/version.txt";
        bool bConDebug = true;
        bool bBypassUpdateCheck = false;
        int iTimerCounter = 0;
        Button ChosenDesk;
        System.Timers.Timer FlashingTimer = new System.Timers.Timer(300);         

        Dictionary<String, Int32[,]> RelativeCoordinateDatabase = new Dictionary<String, Int32[,]>() {
	        {"2_VL", new int[2, 2] {
		        {-14, -32},
		        {-26, 8}
	        }},
	        {"2_V", new int[2, 2] {
		        {-20, -32},
		        {-20, 8}
	        }},
	        {"2_VR", new int[2, 2] {
		        {-26, -32},
		        {-14, 8}
	        }},
	        {"2_HL", new int[2, 2] {
		        {-32, -10},
		        {8, -22}
	        }},
	        {"2_H", new int[2, 2] {
		        {-32, -16},
		        {8, -16}
	        }},
	        {"2_HR", new int[2, 2] {
		        {-32, -22},
		        {8, -10}
	        }},
	        {"4_SL", new int[4, 2] {
		        {-32, -28},
		        {8, -36},
		        {-32, 12},
		        {8, 4}
	        }},
	        {"4_S", new int[4, 2] {
		        {-32, -32},
		        {8, -32},
		        {-32, 8},
		        {8, 8}
	        }},
	        {"4_SR", new int[4, 2] {
		        {-32, -36},
		        {8, -28},
		        {-32, 4},
		        {8, 12}
	        }},
	        {"6_VL", new int[6, 2] {
		        {-28, 28},
		        {12, 28},    
		        {-32, -12},
		        {8, -12},
		        {-36, -52},
		        {4, -52}
	        }},
	        {"6_V", new int[6, 2] {
		        {-32, -52},
		        {8, -52},
		        {-32, -12},
		        {8, -12},
		        {-32, 28},
		        {8, 28}
	        }},
	        {"6_VR", new int[6, 2] {
		        {-36, 28},
		        {4, 28},    
		        {-32, -12},
		        {8, -12},
		        {-28, -52},
		        {12, -52}
	        }},
	        {"6_HL", new int[6, 2] {
		        {-52, -28},
		        {-52, 12},
		        {-12, -32},
		        {-12, 8},
		        {28, -36},
		        {28, 4}
	        }},
	        {"6_H", new int[6, 2] {
		        {-52, -32},
		        {-52, 8},
		        {-12, -32},
		        {-12, 8},
		        {28, -32},
		        {28, 8}
	        }},
	        {"6_HR", new int[6, 2] {
		        {-52, -36},
		        {-52, 4},
		        {-12, -32},
		        {-12, 8},
		        {28, -28},
		        {28, 12}
	        }}
        };
        // Relative coordinates in relation to the marker for each group layout.
        // Used in group creation.

        const Int16 iMarkerCount = 28;
        // Number of markers on the form.
        // Used to populate MarkersList.
        // Never changed in normal program usage.

        List<Button> MarkersList = new List<Button>();
        // List of all markers on the form.
        // Populated upon form initialization, members never changed thereafter.
        // Used to locate markers which are set to invisible upon removing a group.

        List<String> ExistingDesksNames = new List<String>();
        // List of the control names of all exisitng desks.
        // Used to iterate through all desks when clearing them.

        List<Int16> AvailableCycleGroups = new List<Int16>();
        // List of available group indecies.
        // Used only if "Pick Each Group Once Before Picking A Group Again" is checked.

        List<List<Button>> AvailableDesks = new List<List<Button>>();
        // Nested List of Available Desks.
        // First index is the group index (Marker Number).
        // Second index is the desk number (Top -> Down, Left -> Right).

        List<List<ButtonDetails>> GroupsDetailsList = new List<List<ButtonDetails>>();
        // Nested list of co-ordinates of existing desks and their control name.
        // Used for saving/loading since controls are not serializable.

        Button LastClickedButton;
        // The button last clicked by the user.
        // Used to determine which marker location to use when placing a new group.
        // Also used to determine which marker to return to a visible state when removing a group.

        Boolean bSavingRequired = false;
        // Whether anything has been updated since the last save.
        // Updated with each action that the user might want to save.

        [Serializable]
        public struct ButtonDetails {
            public String sName;
            public Int32 xPos;
            public Int32 yPos;

            public ButtonDetails(String sNameArg, Int32 ixPosArg, Int32 iyPosArg) {
                sName = sNameArg;
                xPos = ixPosArg;
                yPos = iyPosArg;
            }
        }
        //-END GLOBAL FORM CLASS VARIABLES-

        //-FORM CLASS FUNCTIONS-
        public SeatRandomizer() {
            /*/
            Parameters: 
                N/A
            Returns:
                N/A
            Notes:
                Form initialization function.
            /*/

            InitializeComponent();
            Preamble();
            // Print the preamble to console establishing that the form has been initialized.

            Text = Text + " [Version "+sSoftwareVersion+"]";
            // Dynamically append the software version to the form's title.

            if (!bBypassUpdateCheck) {
                if (bConDebug) Console.WriteLine("Downloading latest version number from server: " + sUpdateServer);
                // [Debug Only] Print the update server being used, just in case something weird happens.
                string sUpdateString = WebParser.DownloadString(sUpdateServer);
                // Download the content of the file designated in global variabled and store it in a string.
                Boolean bUpdateRequired = Double.Parse(sUpdateString) > Double.Parse(sSoftwareVersion);
                // Compare the current version number to the retrieved one, and store the result for the next few lines.
                if (bConDebug) {
                    Console.WriteLine("Current Version Number: " + sSoftwareVersion);
                    Console.WriteLine("Retrieved Version Number: " + sUpdateString);
                    if (bUpdateRequired) { Console.WriteLine("Detected latest version from server is higher than current version. Notifying user."); }
                    else { Console.WriteLine("Detected current version is equivalent (or higher in case of development) to latest version. Continuing with startup."); }
                    Console.WriteLine("-------------------------");
                }
                // [Debug Only] Print the current and retreived version number, and the result of the comparison.
                if (bUpdateRequired) MessageBox.Show("There is a new version available. If you would like to update, you can select the 'Check For Updates' option under 'Misc'.", "Update Notification");
                // If the current version number is below the retrieved one, notify the user of update procedure.
            }

            for (int i = 0; i < iMarkerCount; i++) {
                AvailableDesks.Add(new List<Button>());
                GroupsDetailsList.Add(new List<ButtonDetails>());
                MarkersList.Add((Button)this.Controls.Find("Marker"+i.ToString(), true)[0]);
            }
            // Populate the globally defined lists.

            FlashingTimer.Elapsed += new ElapsedEventHandler(FlashingTimerElapsedExec);
            // Add the flashing timer function to the timer handler.
        }
        private Int16 SelectRandomGroup() {
            /*/
            Parameters: 
                N/A
            Returns:
                iSelectedGroupNum [Int16] - Number selected from the list of possible desks, either the list of desks not chosen in this cycle (AvailableCycleGroups) or a local list generated with all groups with available desks (NonFullGroups).
            Notes:
                None.
            /*/

            Int16 iSelectedGroupNum;
            //Initialize our variable for returning so that changes made in control structures are available to return outside of them.

            if (CheckBox_AllBeforeRechoose.Checked) {
            // If "Pick Each Group Once Before Picking A Group Again" is checked.
                if (AvailableCycleGroups.Count == 0) {
                    for (int i = 0; i < AvailableDesks.Count; i++) {
                        if (AvailableDesks[i].Count != 0) {
                            AvailableCycleGroups.Add(Convert.ToInt16(i));
                        }
                    }
                }
                // If the cycle is complete (all desks have been chosen once), repopulate our list so they can be chosen again.
                iSelectedGroupNum = AvailableCycleGroups[RandomGen.Next(AvailableCycleGroups.Count)];
                // Randomly choose our group from the list of groups that haven't been chosen this cycle.
                AvailableCycleGroups.Remove(iSelectedGroupNum);
                // Remove the selected group from the list so it isn't chosen again this cycle.
                if (bConDebug) {
                    Console.Write(iSelectedGroupNum.ToString() + " Selected, Remaining Groups: ");
                    foreach (Int16 sGroupNum in AvailableCycleGroups) Console.Write(sGroupNum.ToString() + ", "); Console.Write("\n");
                }
                // [Debug Only] Print the chosen group, and the remaining groups in the list for this cycle.
            } else {
                // If "Pick Each Group Once Before Picking A Group Again" is not checked.
                List<Int16> NonFullGroups = new List<Int16>();
                // Initialize a local list for us to store possible choices.
                for (Int16 i = 0; i < AvailableDesks.Count; i++) if (AvailableDesks[i].Count != 0) NonFullGroups.Add(i);
                // Populate the local list with the possible choices.
                iSelectedGroupNum = NonFullGroups[RandomGen.Next(NonFullGroups.Count)];
                // Randomly choose our group.
                if (bConDebug) Console.Write(iSelectedGroupNum.ToString() + " Selected.");
                // [Debug Only] Print the chosen group.
            }
            return iSelectedGroupNum;
            // Return our chosen group number.
        }
        private Button SelectRandomDeskInGroup(Int16 iGroupNumber) {
            /*/
            Parameters: 
                iGroupNumber [Int16] - The group to choose a desk in. The possible desks in the group are derrived from the AvailableDesks nested list.
            Returns:
                SelectedDesk [Button] - The desk selected from the relevant nested list.
            Notes:
                None.
            /*/

            Button SelectedDesk = AvailableDesks[iGroupNumber][RandomGen.Next(AvailableDesks[iGroupNumber].Count)];
            // Randomly select a desk number in the group number passed in as an argument and derive the desk's button object using the AvailableDesks list.
            AvailableDesks[iGroupNumber].Remove(SelectedDesk);
            // Remove the desk from the list of available desks.
            if (bConDebug) {
                Console.Write(SelectedDesk.Name + " Selected, Remaining Desks: ");
                foreach (Button aButton in AvailableDesks[iGroupNumber]) { Console.Write(aButton.Name + ", "); }
                Console.Write("\n---\n");
            }
            // [Debug Only] Print the chosen desk, and the remaining desks in this group that aren't chosen or marked empty.
            return SelectedDesk;
            // Return our selected desk as the object.
        }
        private void Button_Randomize_Click(object sender, EventArgs e) {
            /*/
            Parameters: 
                Standard event handler parameters.
            Returns:
                N/A
            Notes:
                Triggered by the "Randomize" button, assigns the text from the text box to a random available desk.
            /*/

            if (CheckBox_AllBeforeRechoose.Enabled) CheckBox_AllBeforeRechoose.Enabled = false;
            // Disable the "Pick Each Group Once Before Picking A Group Again" checkbox to lock the user into their choice until the selections are cleared.
            
            if (TextBox_Randomize.Text == "") {
            // // If the user hasn't chosen a question number to put in the chosen box.
                MessageBox.Show("You have not designated a question number.", "Error!");
                // Inform them they must do so, or else the whole program is kind of pointless.
            } else if (TextBox_Randomize.Text.Length > 4 && CheckBox_LengthLimit.Checked == false) {
            // If the question number is greater than 4 characters and the user hasn't checked the override.
                MessageBox.Show("Your question number may not fit on a desk, check the override box to ignore.", "Error!");
                // Inform them of the override's presence and that their question number may not fit on a desk.
            } else if (iTimerCounter == 0) {
            // If our abort conditions aren't met and the last selection isn't still flashing.
                Boolean bDesksAvailable = false;
                // Establish a baseline of no available desks. It must be established here or else it won't be available when we check for it below since it is changed in a control structure.
                foreach (List<Button> SubList in AvailableDesks) if (SubList.Count != 0) bDesksAvailable = true;
                // Check if there are any available desks, and if there are, change the boolean to reflect this. 
                if (!bDesksAvailable) {
                // If there aren't any available desks.
                    MessageBox.Show("There are no seats left!", "Error!");
                    // Notify the user.
                    if (!CheckBox_AllBeforeRechoose.Enabled) CheckBox_AllBeforeRechoose.Enabled = true;
                    // Reenable this since we didn't actually assign anything.
                } else {
                // If there are available desks.
                    ChosenDesk = SelectRandomDeskInGroup(SelectRandomGroup());
                    // Assign our random desk to a global variable so it can be manipulated in the timer function.
                    ChosenDesk.Text = TextBox_Randomize.Text;
                    // Set the text on the desk to the question indicated.
                    if (CheckBox_FlashChosen.Checked) {
                    // If the relevant checkbox is checked.
                        FlashingTimer.Start();
                        // Start our timer. How long it flashes is handled in it's elapsed function.
                    }
                }
            }
        }
        private void FlashingTimerElapsedExec(object sender, EventArgs e) {
            /*/
            Parameters: 
                Standard event handler parameters.
            Returns:
                N/A
            Notes:
                Triggered when FlashingTimer elapses.
            /*/
            
            if (ChosenDesk.BackColor == Color.Azure) {
                ChosenDesk.BackColor = Color.Green;
            } else if (ChosenDesk.BackColor == Color.Green) {
                ChosenDesk.BackColor = Color.Azure;
            }
            // Toggle the colour between azure and green on each cycle of the timer.

            iTimerCounter += 1;
            // Add one to the counter so we can control how many flashes occur.

            if (iTimerCounter > 5) {
                FlashingTimer.Stop();
                iTimerCounter = 0;
                // Once the counter reaches our defined number (including changes to green and changes to white), stop the timer and reset the counter.
            }
            
        }
        private void Button_Clear_Click(object sender, EventArgs e) {
            /*/
            Parameters: 
                Standard event handler parameters.
            Returns:
                N/A
            Notes:
                Triggered by the "Clear" button, clears the text from any desks that have been selected and repopulates the AvailableDesks list.
                Does not change desks marked as empty (red), or remove any groups.
            /*/

            if (!CheckBox_AllBeforeRechoose.Enabled) CheckBox_AllBeforeRechoose.Enabled = true;
            // If "Pick Each Group Once Before Picking A Group Again" is disabled (which it is during the first randomization, to stop the user from messing with our code logic), re-enable it.
            AvailableCycleGroups.Clear();
            // Clear our list of groups used if "Pick Each Group Once Before Picking A Group Again" is checked.
            foreach (List<Button> SubList in AvailableDesks) SubList.Clear();
            // Clear our nested lists of available desks.
            foreach (String Name in ExistingDesksNames) {
            // Since this list is only changed when the layout is, this will iterate through the names of every desk on the form.
                Button ButtonToClear = (Button)this.Controls.Find(Name, true)[0];
                // Derive the button for this iteration from it's name.
                ButtonToClear.Text = "";
                // Clear the text of the button if there is any.
                if (ButtonToClear.BackColor != Color.Firebrick) {
                // If the desk isn't marked as empty.
                    Int16 GroupNumber = Convert.ToInt16(Name.Split('_')[0]);
                    // Derive the desk's group number from its name.
                    AvailableDesks[GroupNumber].Add(ButtonToClear);
                    // Add the desk object to the nested list of available desks in its group.
                }
            }
        }
        private void DeskClickHandler(object sender, MouseEventArgs e) {
            /*/
            Parameters: 
                Standard event handler parameters.
            Returns:
                N/A
            Notes:
                Triggered by clicking on any desk.
                Triggers the appropriate function based on whether the click is with the left or right mouse button.
            /*/

            LastClickedButton = (Button)sender;
            // Since if the user chooses to remove the desk, the object passed to that function will be the option on the context menu, we have to establish the relevant desk globally.
            if (bConDebug) Console.WriteLine("Click detected. Button Name: "+LastClickedButton.Name);
            // [Debug Only] Print the name of the clicked desk.
            if (e.Button == System.Windows.Forms.MouseButtons.Right) {
            // If the click is with the right mouse button.
                ContextMenu_Desk.Show(Cursor.Position);
                // Show the relevant context menu.
            } else if (e.Button == System.Windows.Forms.MouseButtons.Left) {
            // If the click is with the left mouse button.
                Desk_LeftClick(sender, e);
                // Execute our left click function, passing the arguments from this function along.
            }
            // Middle mouse button has no function for us.
        }
        private void Desk_LeftClick(object sender, EventArgs e) {
            /*/
            Parameters: 
                Standard event handler parameters.
            Returns:
                N/A
            Notes:
                Triggered by left clicking on any desk after passing through DeskClickHandler.
                Toggles the desk between occupied (white) and empty (red) states.
            /*/

            Button SenderButton = (Button)sender;
            // Cast the sender as a button, or else button-specific attributes won't be available.
            Int16 GroupNum = Convert.ToInt16(SenderButton.Name.Split('_')[0]);
            // Derive the group number from the desk's name. 
            if (SenderButton.BackColor == Color.Firebrick) {
            // If the desk is marked as empty.
                SenderButton.BackColor = Color.Azure;
                // Change the color to reflect the toggled state of occupied.
                AvailableDesks[GroupNum].Add(SenderButton);
                // Add the desk as available to be chosen.
            } else if (SenderButton.Text == "") {
            // If the desk is marked as occupied.                                                     
                SenderButton.BackColor = Color.Firebrick;
                // Change the color to reflect the toggled state of empty.
                AvailableDesks[GroupNum].Remove(SenderButton);
                // Remove the desk from the list of desks available to be chosen.
            }
        }
        private void MarkerClick(object sender, EventArgs e) {
            /*/
            Parameters: 
                Standard event handler parameters.
            Returns:
                N/A
            Notes:
                Triggered by clicking on any marker.
                Opens the context menu for the user to choose a layout for the group.
            /*/

            if (CheckBox_AllBeforeRechoose.Enabled) {
            // Shortcut to check if we are in the middle of assigning answers. Adding another desk once some have been chosen could mess with the balance of answers per group.
                LastClickedButton = (Button)sender;
                // Since when the user chooses a layout for the group, the object passed to that function will be the option on the context menu, we have to establish the relevant marker globally.
                ContextMenu_Marker.Show(Cursor.Position);
                // Show the relevant context menu.
            } else {
            // If we are in the middle of assigning answers.
                MessageBox.Show("You can't add a group while you're in the middle of assigning answers. Please click clear to clear all assigned answers before modifying the layout.", "Error!");
                // Let the user know that they can't make layout changes now, and why.
            }
        }
        private void Button_CreateGroup_Setup(object sender, EventArgs e) {
            /*/
            Parameters: 
                Standard event handler parameters.
            Returns:
                N/A
            Notes:
                Triggered by clicking on any layout option in the marker context menu.
                Sets up the prerequisites for group creation then calls the creation function.
            /*/

            ToolStripMenuItem SenderContextButton = (ToolStripMenuItem)sender;
            // Cast the sender as a tool strip button, or else tool strip button-specific attributes won't be available.
            String RelevantSubString = SenderContextButton.Name.Substring(28);
            // Cut the relevant (unique) section off of the button's name.
            if (bConDebug) Console.WriteLine("Relevant Layout Details: "+RelevantSubString);
            // [Debug Only] Print the relevant layout details.
            Int32[,] iRelativeCoOrdinates = RelativeCoordinateDatabase[RelevantSubString];
            if (bConDebug) {
                for (int i0 = 0; i0 < iRelativeCoOrdinates.GetLength(0); i0++) {
                    for (int i1 = 0; i1 < iRelativeCoOrdinates.GetLength(1); i1++) {
                        Console.Write(iRelativeCoOrdinates[i0, i1] + " ");
                    }
                    Console.Write("\n");
                }
            }
            // [Debug Only] Print the coordinates fetched from the global database.
            CreateGroup(iRelativeCoOrdinates);
            // Actually create the group.
        }
        private void CreateGroup(Int32[,] ButtonCoordinates) {
            /*/
            Parameters: 
                ButtonCoordinates - The table of coordinates at which to create buttons.
            Returns:
                N/A
            Notes:
                Triggered by clicking on any layout option in the marker context menu.
                Creatres the group using the data established in Button_CreateGroup_Setup.
            /*/

            Int16 GroupNum = Convert.ToInt16(LastClickedButton.Name.Substring(6));
            // Derive the group number from the name of the marker currently stored in LastClickedButton.

	        LastClickedButton.Visible = false;
            // Hide the marker while the group is on the form. It is unhidden upon removal of the group in question.

	        List<Button> GroupCreationList = new List<Button>();
	        for (int i = 1; i <= ButtonCoordinates.GetLength(0); i++) GroupCreationList.Add(new Button());
            // Create and populate the list of button objects with default buttons.

	        for (int i = 0; i < GroupCreationList.Count; i++) {
                GroupCreationList[i].Name = LastClickedButton.Name.Substring(6) + "_" + (i).ToString();
		        GroupCreationList[i].BackColor = Color.Azure;
                GroupCreationList[i].SetBounds(LastClickedButton.Location.X+ButtonCoordinates[i, 0], LastClickedButton.Location.Y+ButtonCoordinates[i, 1], 40, 40);
		        GroupCreationList[i].MouseUp += DeskClickHandler;
	        }
            // Change the relevant default attributes of each button in the list to what we want them to be.

	        foreach (Button NewDesk in GroupCreationList) {
		        if (bConDebug) Console.Write(NewDesk.Name+" ");
                // [Debug Only] Print the names of all the buttons we're going to add to the form.

                SeatRandomizerGroupBox.Controls.Add(NewDesk);
                // Add the button to the form, parented to the groupbox which encompasses all other controls.

                AvailableDesks[GroupNum].Add(NewDesk);
                // Add the desk to the relevant nested list for the group. As all desks in the group default to occupied, we don't have to filter any out, we can remove them if the user clicks on them after creation.

                GroupsDetailsList[GroupNum].Add(new ButtonDetails(NewDesk.Name, NewDesk.Location.X, NewDesk.Location.Y));
                // Add the desk to the relevant nested list of details for the group, for purposes of exporting and importing layouts.

		        ExistingDesksNames.Add(NewDesk.Name);
                // Add the desk's name to the list of all desk names.
	        }
            if (bConDebug) Console.Write("\n");
            // [Debug Only] End the line we started in the foreach loop.

            bSavingRequired = true;
            // Tell the program that a save is required to the current layout.
        }
        private void RemoveGroup(object sender, EventArgs e) {
            /*/
            Parameters: 
                Standard event handler parameters.
            Returns:
                N/A
            Notes:
                Triggered by clicking on the remove group button in the desk context menu.
                Creates the group using the data established in Button_CreateGroup_Setup.
            /*/

            if (CheckBox_AllBeforeRechoose.Enabled) {
            // Shortcut to check if we are in the middle of assigning answers. Adding another desk once some have been chosen could mess with the balance of answers per group.
                Int16 GroupNum = Convert.ToInt16(LastClickedButton.Name.Split('_')[0]);
                // Derive the group number from the name of the marker currently stored in LastClickedButton.

                Button GroupMarker = (Button)this.Controls.Find("Marker" + LastClickedButton.Name.Split('_')[0], true)[0];
                GroupMarker.Visible = true;
                // Find our hidden marker for this group and unhide it.

                List<Button> GroupRemovalList = new List<Button>();
                foreach (String Name in ExistingDesksNames) {
                    if (Convert.ToInt16(Name.Split('_')[0]) == GroupNum) {
                    // If the group number in the desk's name matches the group we're looking for.
                        Button FoundButton = (Button)this.Controls.Find(Name, true)[0];
                        // Derive the button from its name.
                        GroupRemovalList.Add(FoundButton);
                        // Add the button to the list of buttons we'll be removing.
                    }
                }
                // Create and populate the list of desks to remove. Since we don't have a reliable way to find ALL desks in a group (only the available ones), we have to find them by the group number in their name.

                foreach (Button TargetButton in GroupRemovalList) {
                    ExistingDesksNames.Remove(TargetButton.Name);
                    // Now that we have the button objects referenced in a list, we can remove their names from the list of desks in preparation to delete them.
                    SeatRandomizerGroupBox.Controls.Remove(TargetButton);
                    // Delete the desk from the groupbox.
                }

                AvailableDesks[GroupNum].Clear();
                // Clear the nested list of availalbe desks for this group. Any assigned or marked empty ones won't be in this list anyways.

                GroupsDetailsList[GroupNum].Clear();
                // Clear the nested list of details for this group so it is removed from the saved layout file next save.

                bSavingRequired = true;
                // Tell the program that a save is required to the current layout.
            } else {
            // If we are in the middle of assigning answers.
                MessageBox.Show("You can't remove a group while you're in the middle of assigning answers. Please click clear to clear all assigned answers before modifying the layout.", "Error!");
                // Let the user know that they can't make layout changes now, and why.
            }
        }
        private void ResetForm() {
            /*/
            Parameters: 
                N/A
            Returns:
                N/A
            Notes:
                Called by both Button_Reset_Click and MenuBar_File_Open_Click.
                Removes all desks/groups from the form.
            /*/

            foreach (List<Button> ThisList in AvailableDesks) ThisList.Clear();
            // Clear all nested lists of available desks since all desks are now removed.

            foreach (List<ButtonDetails> ThisList in GroupsDetailsList) ThisList.Clear();
            // Clear all nested lists of desk details.

            foreach (String Name in ExistingDesksNames) {
                Button FoundButton = (Button)this.Controls.Find(Name, true)[0];
                SeatRandomizerGroupBox.Controls.Remove(FoundButton);
            }
            // Remove all desks from the form.

            foreach (Button ThisButton in MarkersList) ThisButton.Visible = true;
            // Make all markers visible.

            AvailableCycleGroups.Clear();
            ExistingDesksNames.Clear();
            // Clear the two relevant lists we haven't yet dealt with.
        }
        private void Button_Reset_Click(object sender, EventArgs e) {
            /*/
            Parameters: 
                Standard event handler parameters.
            Returns:
                N/A
            Notes:
                Triggered by clicking on the "Reset" button.
                Calls the ResetForm function to remove all desks/groups.
            /*/
            
            ResetForm();
            // Since the contents of ResetForm are needed by MenuBar_File_Open_Click as well, it is in a separate function.
        }
        
        private void MenuBar_File_SaveAs_Click(object sender, EventArgs e) {
            /*/
            Parameters: 
                Standard event handler parameters.
            Returns:
                N/A
            Notes:
                Triggered by clicking on the "Save As.." button.
                Opens the save file dialog and executes the save procedure.
            /*/
            
            if (SaveFileDialog.ShowDialog() == DialogResult.OK) {
                try {
                    FileStream FileParser = new FileStream(SaveFileDialog.FileName, FileMode.Create, FileAccess.Write);
                    // Initialize the file stream.

                    BinarySerializer.Serialize(FileParser, GroupsDetailsList);
                    // Write the details.

                    FileParser.Close();
                    // Close the file stream.

                    if (bConDebug) Console.WriteLine("Serialization Done, No Errors.");
                    // [Debug Only] Print confirmation of successful serialization.
                    
                    OpenFileDialog.FileName = SaveFileDialog.FileName;
                    // Update the other dialog's file name.

                    bSavingRequired = false;
                    // Since we've saved, the file is up to date.
                } catch (Exception exception) {
                    MessageBox.Show(exception.Message, "BinarySerializer Serialization Error");
                }
            }
        }
        private void MenuBar_File_Save_Click(object sender, EventArgs e) {
            /*/
            Parameters: 
                Standard event handler parameters.
            Returns:
                N/A
            Notes:
                Triggered by clicking on the "Save" button.
                Executes the save procedure to the most recent path, and opens the save file dialog if a path isn't defined.
            /*/
            
            if (SaveFileDialog.FileName == "") {
                MenuBar_File_SaveAs_Click(sender, e);
                // Call the "Save As.." function that has the code necessary for dealing with the dialog.
            } else {
                try {
                    FileStream FileParser = new FileStream(SaveFileDialog.FileName, FileMode.Create, FileAccess.Write);
                    // Initialize the file stream.

                    BinarySerializer.Serialize(FileParser, GroupsDetailsList);
                    // Write the details.

                    FileParser.Close();
                    // Close the file stream.

                    if (bConDebug) Console.WriteLine("Serialization Done, No Errors.");
                    // [Debug Only] Print confirmation of successful serialization.

                    bSavingRequired = false;
                    // Since we've saved, the file is up to date.
                } catch (Exception exception) {
                    MessageBox.Show(exception.Message, "BinarySerializer Serialization Error");
                }
            }
        }
        private void MenuBar_File_Open_Click(object sender, EventArgs e) {
            /*/
            Parameters: 
                Standard event handler parameters.
            Returns:
                N/A
            Notes:
                Triggered by clicking on the "Open" button.
                Opens the open file dialog and executes the open procedure.
            /*/
            
            if (OpenFileDialog.ShowDialog() == DialogResult.OK) {
                try {
                    ResetForm();
                    FileStream FileParser = new FileStream(OpenFileDialog.FileName, FileMode.Open, FileAccess.Read);
                    // Initialize the file stream.

                    GroupsDetailsList = (List<List<ButtonDetails>>)BinarySerializer.Deserialize(FileParser);
                    // Read the file.

                    FileParser.Close();
                    // Close the file stream.

                    for (Int32 i = 0; i < GroupsDetailsList.Count; i++) {
                        if (bConDebug) Console.WriteLine(i.ToString());
                        // [Debug Only] Print out each desk we're recreating.

                        if (GroupsDetailsList[i].Count > 0) {
                            if (bConDebug) Console.WriteLine("Marker" + GroupsDetailsList[i][0].sName.Split('_')[0]);
                            // [Debug Only] Print out the marker for the group number we're handling.

                            foreach (ButtonDetails DetailsToRecreate in GroupsDetailsList[i]) {
                                Button ButtonToRecreate = new Button();
                                // Instantiate the button.

                                ButtonToRecreate.Name = DetailsToRecreate.sName;
                                ButtonToRecreate.BackColor = Color.Azure;
                                ButtonToRecreate.SetBounds(DetailsToRecreate.xPos, DetailsToRecreate.yPos, 40, 40);
                                ButtonToRecreate.MouseUp += DeskClickHandler;
                                // Change the relevant default attributes of each button in the list to what we want them to be based on the saved details.

                                AvailableDesks[i].Add(ButtonToRecreate);
                                // Add the desk to the relevent nested list.

                                ExistingDesksNames.Add(ButtonToRecreate.Name);
                                // Add the desk's name to the master list of names.

                                SeatRandomizerGroupBox.Controls.Add(ButtonToRecreate);
                                // Add the button to the form.
                            }
                            
                            Button GroupMarker = (Button)this.Controls.Find("Marker" + GroupsDetailsList[i][0].sName.Split('_')[0], true)[0];
                            // Derive the relevant marker from its name.

                            GroupMarker.Visible = false;
                            // Hide the marker.
                        }
                    }
                    if (bConDebug) Console.WriteLine("Deserialization Done, No Errors.");
                    // [Debug Only] Print confirmation of successful deserialization.

                    SaveFileDialog.FileName = OpenFileDialog.FileName;
                    // Update the other dialog's file name.

                    bSavingRequired = false;
                    // Since we've just openned a new file, the file is up to date.
                } catch (Exception exception) {
                    MessageBox.Show(exception.Message, "BinarySerializer Deserialization Error");
                }
            }
        }
        private void MenuBar_Misc_Updates_Click(object sender, EventArgs e) {
            /*/
            Parameters: 
                Standard event handler parameters.
            Returns:
                N/A
            Notes:
                Checks for updates and executes the update if an update is found.
                Triggered by clicking the "Check for Updates" button.
            /*/

            if (!bBypassUpdateCheck) {
                if (bConDebug) Console.WriteLine("Downloading latest version number from server: " + sUpdateServer);
                // [Debug Only] Print the update server being used, just in case something weird happens.
                string sUpdateString = WebParser.DownloadString(sUpdateServer);
                // Download the content of the file designated in global variabled and store it in a string.
                Boolean bUpdateRequired = Double.Parse(sUpdateString) > Double.Parse(sSoftwareVersion);
                // Compare the current version number to the retrieved one, and store the result for the next few lines.
                if (bConDebug) {
                    Console.WriteLine("Current Version Number: " + sSoftwareVersion);
                    Console.WriteLine("Retrieved Version Number: " + sUpdateString);
                    if (bUpdateRequired) { Console.WriteLine("Detected latest version from server is higher than current version. Notifying user."); } else { Console.WriteLine("Detected current version is equivalent (or higher in case of development) to latest version. Continuing with startup."); }
                    Console.WriteLine("-------------------------");
                }
                // [Debug Only] Print the current and retreived version number, and the result of the comparison.
                if (bUpdateRequired) {
                    DialogResult UpdateDialogResult = MessageBox.Show("There is a new version available, would you like to update?\nAll unsaved changes will be lost.", "Update?", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                    // If the current version number is below the retrieved one, prompt the user to update.
                    if (UpdateDialogResult == DialogResult.Yes) {
                        ProcessStartInfo UpdaterParameters = new ProcessStartInfo();
                        // Initialize the structure that defines the program start parameters.

                        UpdaterParameters.Arguments = '"'+Application.ExecutablePath+'"';
                        // Argument to the program. In this case the file to update (this program).

                        UpdaterParameters.Verb = "runas";
                        // Elevate the updater so it has rights to overwrite this program.

                        UpdaterParameters.FileName = Application.StartupPath+"\\Updater.exe";
                        // Path to the updater. Since it's in the same folder we can use this program's location to set it dynamically.

                        UpdaterParameters.WindowStyle = ProcessWindowStyle.Normal;
                        UpdaterParameters.CreateNoWindow = false;
                        // Configure it to show a console window for the updater.

                        using (Process UpdaterProcess = Process.Start(UpdaterParameters));
                        // Start the updater.

                        Application.Exit();
                        // Hard-close this program, since we prompted the user to save when they were confirming the update.
                    }
                } else {
                    MessageBox.Show("Your version is up to date!", "No update found.");
                }
            }
        }
        private void MenuBar_Misc_Credits_Click(object sender, EventArgs e) {
            /*/
            Parameters: 
                Standard event handler parameters.
            Returns:
                N/A
            Notes:
                Displays a messagebox with the credits information.
                Triggered by clicking the "Credits" button.
            /*/
            
            MessageBox.Show("-----------------------------------\nSeat Randomizer Version "+sSoftwareVersion+"\n-----------------------------------\n\nProgramming & Development:\n     Jameson R. Grieve\n     JamesonRGrieve@GMail.com\n\nUses Microsoft .Net Framework", "Credits");
        }
        private void SeatRandomizer_FormClosing(object sender, FormClosingEventArgs e) {
            /*/
            Parameters: 
                Standard form closing parameters.
            Returns:
                N/A
            Notes:
                Called on program exit to check for unsaved data.
            /*/
            
            if (bSavingRequired) {
                DialogResult ExitDialogResult = MessageBox.Show("Would you like to save your changes? Active edits will not be saved, please either create a new entry or overwrite an existing one if you have made changes.", "Save?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Stop);
                if (ExitDialogResult == DialogResult.Yes) {
                    if (SaveFileDialog.FileName == "") {
                        MenuBar_File_SaveAs_Click(sender, e);
                        // Call the "Save As.." function that has the code necessary for dealing with the dialog.
                    } else {
                        try {
                            FileStream FileParser = new FileStream(SaveFileDialog.FileName, FileMode.Create, FileAccess.Write);
                            // Initialize the file stream.

                            BinarySerializer.Serialize(FileParser, GroupsDetailsList);
                            // Write the details.

                            FileParser.Close();
                            // Close the file stream.

                            if (bConDebug) Console.WriteLine("Serialization Done, No Errors.");
                            // [Debug Only] Print confirmation of successful serialization.

                            bSavingRequired = false;
                            // Kind of irrelevent since if we're here the form is about to close, but for the sake of symmetry let's set this.
                        } catch (Exception exception) {
                            MessageBox.Show(exception.Message, "BinarySerializer Serialization Error");
                        }
                    }
                } else if (ExitDialogResult == DialogResult.Cancel) {
                    e.Cancel = true;
                }
            }
        }
    }
}
//-END FORM NAMESPACE-
